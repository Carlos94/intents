package com.example.dodigital.intent;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class ThirdActivity extends AppCompatActivity {

    private EditText editTextPhone;
    private EditText editTextWeb;
    private ImageButton imgBtnPhone;
    private ImageButton imgBtnWeb, imgBtnCamara;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        editTextPhone = findViewById(R.id.editTelefono);
        editTextWeb = findViewById(R.id.editWeb);
        imgBtnPhone = findViewById(R.id.btnTelefono);
        imgBtnWeb = findViewById(R.id.btnWeb);
        imgBtnCamara = findViewById(R.id.btnCamara);

        imgBtnPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phoneNumber = editTextPhone.getText().toString();

                if (phoneNumber != null){

                }
            }

            private void  OlderVersions(String phoneNumber){
                Intent intentCall = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+phoneNumber));
                if (CheckPermission(Manifest.permission.CALL_PHONE)){
                    startActivity(intentCall);
                }else{
                    Toast.makeText(ThirdActivity.this,"No tienes permiso",Toast.LENGTH_LONG).show();
                }

            }

            private void NewerVersions(){

            }

        });


    }
    public boolean CheckPermission(String permisssion){
        int result = this.checkCallingOrSelfPermission(permisssion);
        return  result == PackageManager.PERMISSION_GRANTED;
    }

}
